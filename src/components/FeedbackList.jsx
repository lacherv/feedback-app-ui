import FeedbackItem from "./FeedbackItem";

const FeedbackList = ({ feedback, handleDelete }) => {
  if (!feedback || feedback.length === 0) {
    return <p>No Feedback Yet.</p>;
  }
  return (
    <div className="feedback-list">
      {feedback.map((it) => (
        <FeedbackItem
          key={it.id}
          item={it}
          handleDelete={handleDelete}
        />
      ))}
    </div>
  );
};

export default FeedbackList;
