import { FaTimes } from "react-icons/fa";
import proTotypes from "prop-types";
import Card from "./shared/Card";

const FeedbackItem = ({ item, handleDelete }) => {
  return (
    <Card>
      <div className="num-display">{item.rating}</div>
      <button className="close" onClick={() => handleDelete(item.id)}>
        <FaTimes color="purple" />
      </button>
      <div className="text-display">{item.text}</div>
    </Card>
  );
};

FeedbackItem.proTotypes = {
  item: proTotypes.object.isRequired,
};

export default FeedbackItem;
